﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCardScript : MonoBehaviour {

    public PlayerController playerController;
    private PlayKeyCardAudio playKeyCardAudio;
    
    private void Start()
    {
        playKeyCardAudio = GameObject.Find("KeyCardAudioSource").GetComponent<PlayKeyCardAudio>();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //check if the collision is with the Player and if so, then check if he has the correct keycard to open the door
        if (collider.tag == "Player")
        {
            playKeyCardAudio.PlaySound();
            playerController.takeKeyCard();
            Destroy(this.gameObject);
        }
    }
}

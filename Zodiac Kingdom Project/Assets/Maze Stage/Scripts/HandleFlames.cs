﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleFlames : MonoBehaviour {

    //variable to check if flames are open or not
    public bool isOpen;

    //variable that refers to HandleButton.cs script
    public HandleButton handleButton;

    //variable to store the animator
    public Animator animator;

    public AudioSource audioSource;

    public AutoWalking autoWalking;
    public HandleLives handleLives;

    // Use this for initialization
    void Start()
    {
        isOpen = true;
        audioSource.loop = true;
    }

    private void Update()
    {
        if (isOpen)
        {
            if (!handleButton.isOpen)
            {
                this.isOpen = false;
                //close flames
                animator.SetBool("isOpen", false);
                //the sound must be closed, too
                audioSource.Stop();
            }
        }
        else
        {
            if (handleButton.isOpen)
            {
                this.isOpen = true;
                //open flames
                animator.SetBool("isOpen", true);
                //the sound must be opened, too
                audioSource.Play();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            LoseLife();
            autoWalking.CharacterGoBack();
        }
    }

    private void LoseLife()
    {
        handleLives.LoseALife();
    }

}

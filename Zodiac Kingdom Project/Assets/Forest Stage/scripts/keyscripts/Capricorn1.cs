﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capricorn1 : MonoBehaviour {
    public AudioSource MusicSource;

    public GameObject capricorn, capricorn1;
    
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D collision)
    {
        MusicSource.Play();

        capricorn.gameObject.SetActive(true);

        capricorn1.gameObject.SetActive(false);
        KeyScript.zwdia += 1;

    }

}

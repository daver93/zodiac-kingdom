﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaurusScript : MonoBehaviour {
    public AudioSource MusicSource;

    public GameObject taurus, taurus1;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D collision)
    {
        MusicSource.Play();

        taurus.gameObject.SetActive(true);

        taurus1.gameObject.SetActive(false);
        KeyScript.zwdia += 1;
    }
}

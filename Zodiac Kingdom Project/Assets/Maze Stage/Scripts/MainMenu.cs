﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void QuitGame()
    {
        Application.Quit();
    }

    public void NewGame()
    {
        Time.timeScale = 1f;
        //SceneManager.LoadScene("Maze");
        SceneManager.LoadScene("IntroScene 01");
    }
}

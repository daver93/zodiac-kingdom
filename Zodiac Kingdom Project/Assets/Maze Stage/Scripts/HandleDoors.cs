﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleDoors : MonoBehaviour
{

    public bool isOpen;
    public Animator animator;
    public Object keyCard;
    public bool hasTheKey;

    public PlayerController playerController;
    public ChangeKeysNumberUI changeKeysNumberUI;

    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        isOpen = false;
        hasTheKey = false;

        audioSource.loop = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //check if the collision is with the Player and if so, then check if he has the correct keycard to open the door
        if (collision.collider.tag == "Player")
        {
            if (checkForKeyCard())
            {
                if (hasTheKey == true)
                {
                    animator.SetBool("hasTheKey", true);
                    OpenDoor();
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            if (isOpen)
            {
                hasTheKey = false;
            }
        }
    }

    //check if the player has a keyCard to open this door
    private bool checkForKeyCard()
    {
        if (playerController.numberOfKeyCardsHolding > 0)
        {
            playerController.numberOfKeyCardsHolding--;
            hasTheKey = true;
            return true;
        }
        return false;
    }

    //change button to on and off vice versa
    private void OpenDoor()
    {
        animator.SetBool("isOpen", false);
        isOpen = !isOpen;
        audioSource.Play();
    }
}

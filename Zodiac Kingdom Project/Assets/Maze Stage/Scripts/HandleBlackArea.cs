﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleBlackArea : MonoBehaviour {

    //variable to check if black is open or not
    public bool isOpen;

    public GameObject blackImage;

    //variable that refers to HandleButton.cs script
    public HandleButton handleButton;

    //variable to store the animator
    public Animator animator;

    // Use this for initialization
    void Start()
    {
        isOpen = true;
    }

    private void Update()
    {
        if (blackImage.activeInHierarchy)
        {
            blackImage.SetActive(false);
        }
        /*
        else
        {
            if (!blackImage.activeInHierarchy)
            {
                blackImage.SetActive(true);
            }
        }
        */
    }
}

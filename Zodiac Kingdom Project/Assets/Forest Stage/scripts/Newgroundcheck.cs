﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Newgroundcheck : MonoBehaviour {

    

    private Player player;

    //Use this for initialization
    void Start()
    {
        player = gameObject.GetComponentInParent<Player>();
    }

    /*void OnTriggerEnter2D(Collider2D col)
    {
        player.grounded = true;
    }
    void OnTriggerStay2D(Collider2D col)
    {
        player.grounded = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        player.grounded = false;
    }*/

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ground")
        {
            player.grounded = true;
        }
    }
    void OnCollisionStay(Collision other)
    {
        
        if(other.gameObject.tag == "ground")
        {
            player.grounded = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "ground")
        {
            player.grounded = false;
        }
    }
}

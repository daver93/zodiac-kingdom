﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoWalking : MonoBehaviour {

    public string destination;
    public bool isAutoWalking;
    public float secondsCount;

    public PlayerController playerController;

    public IEnumerator AutoWalk(string destination)
    {
        secondsCount = 0;

        float time = 0.3f;
        isAutoWalking = true;

        while (secondsCount < time)
        {

            if (destination == "Up")
            {
                playerController.WalkDownward();
            }
            else if (destination == "Down")
            {
                playerController.WalkUpward();
            }
            else if (destination == "Left")
            {
                playerController.WalkRight();
            }
            else if (destination == "Right")
            {
                playerController.WalkLeft();
            }

            secondsCount += Time.deltaTime;
            yield return null;
        }

        isAutoWalking = false;

        secondsCount = 0;
        playerController.NotWalking();
    }

    public void CharacterGoBack()
    {
        if (playerController.GetCharacterDestination() == "Up")
        {
            StartCoroutine(this.AutoWalk("Up"));
        }
        else if (playerController.GetCharacterDestination() == "Down")
        {
            StartCoroutine(this.AutoWalk("Down"));
        }
        else if (playerController.GetCharacterDestination() == "Left")
        {
            StartCoroutine(this.AutoWalk("Left"));
        }
        else if (playerController.GetCharacterDestination() == "Right")
        {
            StartCoroutine(this.AutoWalk("Right"));
        }
    }
}

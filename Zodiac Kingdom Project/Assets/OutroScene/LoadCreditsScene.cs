﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadCreditsScene : MonoBehaviour {

    public float secondsCount;

    private void Start()
    {
        StartCoroutine(this.WaitToLoadNextScene());
    }

    public IEnumerator WaitToLoadNextScene()
    {
        secondsCount = 0;

        float time = 5f;

        while (secondsCount < time)
        {
            secondsCount += Time.deltaTime;
            yield return null;
        }
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene("Credits");
    }
}

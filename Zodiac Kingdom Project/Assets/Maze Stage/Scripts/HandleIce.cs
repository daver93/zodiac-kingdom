﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleIce : MonoBehaviour {

    //variable to check if ice is open or not
    public bool isOpen;

    //variable that refers to HandleButton.cs script
    public HandleButton handleButton;

    //variable to store the animator
    public Animator animator;

    // Use this for initialization
    void Start()
    {
        isOpen = true;
    }

    private void Update()
    {
        if (isOpen)
        {
            if (!handleButton.isOpen)
            {
                this.isOpen = false;
                //close ice
                animator.SetBool("isOpen", false);
            }
        }
        /*
        else
        {
            if (handleButton.isOpen)
            {
                this.isOpen = true;
                //open ice
                animator.SetBool("isOpen", true);
            }
        }
        */
    }
}

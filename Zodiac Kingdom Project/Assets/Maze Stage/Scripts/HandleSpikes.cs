﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleSpikes : MonoBehaviour {

    //variable to check if spikes are open (up) or not
    public bool isOpen;

    public bool isAutoWalking;

    //variable that refers to HandleButton.cs script
    //public HandleButton handleButton;

    //variable to store the animator
    public Animator animator;

    public PlayerController playerController;
    public HandleLives handleLives;
    public TimeCounter timeCounter;

    private float secondsCount;

    public AutoWalking autoWalking;

    // Use this for initialization
    void Start()
    {
        isOpen = true;
        isAutoWalking = false;
        //audioSource.loop = true;
    }

    private void Update()
    {
        SpikesAutoMovement();
        isAutoWalking = autoWalking.isAutoWalking;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check if the collision is with the Player and if spikes are open (up)
        if (collision.tag == "Player")
        {

            if (this.isOpen)
            {
                autoWalking.CharacterGoBack();
                
                Debug.Log("Player has to lose one life");
            }
            else if (!this.isOpen)
            {
                
            }
            LoseLife();
        }
    }

    private void LoseLife()
    {
        handleLives.LoseALife();
    }

    public void SpikesAutoMovement()
    {
        int getCurrentSecond = Mathf.RoundToInt(timeCounter.GetSeconds());
        if (getCurrentSecond % 2 == 0)
        {
            this.isOpen = false;
            animator.SetBool("isOpen", false);

        }
        else if (getCurrentSecond % 2 == 1)
        {
            this.isOpen = true;
            animator.SetBool("isOpen", true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayZodiacKeyAudio : MonoBehaviour {

    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource.loop = false;
    }

    public void PlaySound()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cancer1script : MonoBehaviour {
    public AudioSource MusicSource;

    public GameObject cancer,  cancer1 ;

    void OnTriggerEnter2D(Collider2D collision)
    {
        MusicSource.Play();

        cancer.gameObject.SetActive(true);
        cancer1.gameObject.SetActive(false);
        KeyScript.zwdia += 1;
    }
}


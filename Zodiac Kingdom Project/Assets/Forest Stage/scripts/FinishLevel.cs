﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLevel : MonoBehaviour {

    private float secondsCount;

    public GameObject door;
    public GameObject green;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check if the collision is with the Player and door is inactive and green button is active
        if (collision.tag == "Player")
        {
            if (!door.activeInHierarchy && green.activeInHierarchy)
            {
                StartCoroutine(this.WaitToLoadNextScene());
            }
            
        }
    }

    public IEnumerator WaitToLoadNextScene()
    {
        secondsCount = 0;

        float time = 1f;

        while (secondsCount < time)
        {
            secondsCount += Time.deltaTime;
            yield return null;
        }
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene("Maze");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeKeysNumberUI : MonoBehaviour {

    public TextMesh keysNumberText;
    public PlayerController playerController;

	// Update is called once per frame
	void Update () {
        keysNumberText.text = playerController.numberOfKeyCardsHolding.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndOfMazeStage : MonoBehaviour {

    public GameObject congratulationText;

    public float secondsCount;

    public PlayerController playerController;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check if the collision is with the Player
        if (collision.tag == "Player")
        {
            //stop time (freeze anything)
            Time.timeScale = 0f;

            HandleFinishMazeStageActions();
        }
    }

    private void HandleFinishMazeStageActions()
    {
        congratulationText.SetActive(true);
        Time.timeScale = 1f;
        StartCoroutine(this.WaitToLoadNextScene());
    }

    public IEnumerator WaitToLoadNextScene()
    {
        secondsCount = 0;

        float time = 0.3f;

        while (secondsCount < time)
        {

            playerController.NotWalking();

            secondsCount += Time.deltaTime;
            yield return null;
        }
        Debug.Log("Next");
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene("OutroScene");
    }

}

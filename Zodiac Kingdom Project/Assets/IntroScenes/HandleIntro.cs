﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandleIntro : MonoBehaviour {

    public float secondsCount;

    private int pageNumber;

    public GameObject image001;
    public GameObject image002;

    private float time;

    private void Start()
    {
        pageNumber = 1;
        image002.SetActive(false);
        StartCoroutine(this.WaitToLoadNextScene(12));
    }

    public IEnumerator WaitToLoadNextScene(float time)
    {
        secondsCount = 0;

        while (secondsCount < time)
        {
            secondsCount += Time.deltaTime;
            yield return null;
        }

        if (pageNumber == 1)
        {
            DisplaySecondIntroStoryImage();
        }
        else
        {
            LoadNextScene();
        }
    }

    private void DisplaySecondIntroStoryImage()
    {
        image001.SetActive(false);
        image002.SetActive(true);
        pageNumber++;
        StartCoroutine(this.WaitToLoadNextScene(15));
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene("ForestScene");
    }
}

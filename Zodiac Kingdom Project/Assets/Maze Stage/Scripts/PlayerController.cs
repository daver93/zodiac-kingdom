﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Code by David Triantafyllou

public class PlayerController : MonoBehaviour {

    private Rigidbody2D rb2d;
    public float speed;

    public Animator animator;

    public int numberOfKeyCardsHolding;

    public ChangeKeysNumberUI changeKeysNumberUI;
    public HandleSpikes handleSpikes;

    public AudioClip keyCardAudio;
    public AudioClip zodiacKeyCardAudio;
    public AudioClip footstepAudio;

    private AudioSource audioSource;

    public bool ariesKey;
    public bool leoKey;
    public bool aquariusKey;
    public bool geminiKey;
    public bool libraKey;
    public bool sagittariusKey;

    public bool muteFootstepSound;

    public Toggle toggle;

    public string characterDestination;

    private PlayZodiacKeyAudio playZodiacKeyAudio;


    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        numberOfKeyCardsHolding = 0;

        audioSource = GetComponent<AudioSource>();

        playZodiacKeyAudio = GameObject.Find("ZodiacKeyAudioSource").GetComponent<PlayZodiacKeyAudio>();

        muteFootstepSound = false;
    }

    private void FixedUpdate()
    {
        if (handleSpikes.isAutoWalking == false)
        {
            if (Input.GetKey("w") || Input.GetKey(KeyCode.UpArrow))
            {
                WalkUpward();
            }
            else if (Input.GetKey("s") || Input.GetKey(KeyCode.DownArrow))
            {
                WalkDownward();
            }
            else if (Input.GetKey("a") || Input.GetKey(KeyCode.LeftArrow))
            {
                WalkLeft();
            }
            else if (Input.GetKey("d") || Input.GetKey(KeyCode.RightArrow))
            {
                WalkRight();
            }
            else
            {
                NotWalking();
            }
        }

        //Player can run while holding shift
        if ( playerIsRunning() )
        {
            speed = 5;
        }
        else
        {
            speed = (float)3;
        }

        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.LeftShift))
        {
            SceneManager.LoadScene("OutroScene");
        }
    }

    public void NotWalking()
    {
        rb2d.velocity = new Vector2(0, 0);

        animator.SetBool("moveUp", false);
        animator.SetBool("moveDown", false);
        animator.SetBool("moveLeft", false);
        animator.SetBool("moveRight", false);

        audioSource.Pause();
    }

    public void WalkUpward()
    {
        rb2d.velocity = new Vector2(0, speed);
        animator.SetBool("moveUp", true);

        animator.SetBool("moveDown", false);
        animator.SetBool("moveLeft", false);
        animator.SetBool("moveRight", false);

        IsFootstepSoundMuted();

        SetCharacterDestination("Up");
    }

    public void WalkDownward()
    {
        rb2d.velocity = new Vector2(0, -speed);
        animator.SetBool("moveDown", true);

        animator.SetBool("moveUp", false);
        animator.SetBool("moveLeft", false);
        animator.SetBool("moveRight", false);

        IsFootstepSoundMuted();

        SetCharacterDestination("Down");
    }

    public void WalkLeft()
    {
        rb2d.velocity = new Vector2(-speed, 0);
        animator.SetBool("moveLeft", true);

        animator.SetBool("moveDown", false);
        animator.SetBool("moveUp", false);
        animator.SetBool("moveRight", false);

        IsFootstepSoundMuted();

        SetCharacterDestination("Left");
    }

    public void WalkRight()
    {
        rb2d.velocity = new Vector2(speed, 0);
        animator.SetBool("moveRight", true);

        animator.SetBool("moveDown", false);
        animator.SetBool("moveLeft", false);
        animator.SetBool("moveUp", false);

        IsFootstepSoundMuted();

        SetCharacterDestination("Right");
    }

    public bool playerIsRunning()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            return true;
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ZodiacKey")
        {
            playZodiacKeyAudio.PlaySound();
            checkZodiacKey(collision.gameObject);
        }
    }

    public void takeKeyCard()
    { 
        numberOfKeyCardsHolding++;
    }

    public void checkZodiacKey(GameObject zodiacKey)
    {
        if (zodiacKey.name == "aries_key")
        {
            ariesKey = true;
        }
        else if (zodiacKey.name == "Leo_key")
        {
            leoKey = true;
        }
        else if (zodiacKey.name == "sagittarius_key")
        {
            sagittariusKey = true;
        }
        else if (zodiacKey.name == "gemini_key")
        {
            geminiKey = true;
        }
        else if (zodiacKey.name == "libra_key")
        {
            libraKey = true;
        }
        else if (zodiacKey.name == "aquarius_key")
        {
            aquariusKey = true;
        }

        Destroy(zodiacKey);
    }

    public void PlayFootstepSound()
    {
        if ( !audioSource.isPlaying )
        {
            if (audioSource.clip != footstepAudio)
            {
                audioSource.clip = footstepAudio;
            }
            //audioSource.Play();
            //audioSource.PlayDelayed((float)0.085);
            audioSource.PlayDelayed((float)0.2);
        }
    }

    public bool IsFootstepSoundMuted()
    {

        if (toggle.isOn)
        {
            //footstep must be heard
            muteFootstepSound = false;
        }
        else
        {
            //footstep must NOT be heard
            muteFootstepSound = true;
        }

        if (muteFootstepSound)
        {
            return true;
        }
        PlayFootstepSound();
        return false;
    }

    //with this method set where the character is looking each time
    private void SetCharacterDestination(string destination)
    {
        if (characterDestination != destination)
        {
            characterDestination = destination;
        }
    }

    //with this method we know where the character is looking each time
    public string GetCharacterDestination()
    {
        return this.characterDestination;
    }
}

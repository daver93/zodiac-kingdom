﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour {

    public TextMesh timerText;
    private float secondsCount;
    private int minuteCount;
    private int hourCount;


    string time;

    void Update()
    {
        UpdateTimerUI();
    }
    
    public void UpdateTimerUI()
    {
        secondsCount += Time.deltaTime;

        if (secondsCount >= 60)
        {
            minuteCount++;
            secondsCount = 0;
        }
        else if (minuteCount >= 60)
        {
            hourCount++;
            minuteCount = 0;
        }

        time = hourCount.ToString("00") + ":" + minuteCount.ToString("00") + ":" + secondsCount.ToString("00");

        timerText.text = time;
    }

    public float GetSeconds()
    {
        return secondsCount;
    }
}

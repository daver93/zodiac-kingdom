﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float maxSpeed = 1;
    public float speed = 1f;
    public float jumpPower = 1f;
    public bool grounded;
    private Rigidbody2D rb2d;
    private Animator anim;
    public AudioSource MusicSource;
    public GameObject keyscompleted;
    
    public GameObject PauseUI;

    private bool paused = false;

    public GameObject GameOver;

    public GameObject heart1, heart2, heart3;
    public static int health;


    // Use this for initialization
    void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();

        PauseUI.SetActive(false);

        health = 3;
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);

        GameOver.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Grounded", grounded);
        anim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb2d.AddForce(Vector2.up * jumpPower);
            MusicSource.Play();
        }
         if(Input.GetButtonDown("Pause"))
        {
            paused = !paused;
        }
        if (paused)
        {
            PauseUI.SetActive(true);
            Time.timeScale = 0;
        }
        if (!paused)
        {
            PauseUI.SetActive(false);
            Time.timeScale = 1;
        }

        if (health == 3)
        {
            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(true);
        }

        if (health == 2)
        {
            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(false);
        }
        if (health == 1)
        {
            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(false);
            heart3.gameObject.SetActive(false);
        }
        if (health == 0)
        {
            heart1.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart3.gameObject.SetActive(false);



            GameOver.SetActive(true);
            Time.timeScale = 0;
            if (Input.GetButtonDown("Pause"))
            {

                GameOver.SetActive(true);

            }



        }

        if (Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.LeftShift))
        {
            SceneManager.LoadScene("Maze");
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        //moveplayer
        rb2d.AddForce((Vector2.right * speed) * h);
        //limit speed of player
        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }

        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }
    }
    public void Resume()
    {
        paused = false;

    }
    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void Quit()
    {
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleStars : MonoBehaviour {

    public TimeCounter timeCounter;

    public GameObject starsImage;

    private float secondsCount;
    private int minuteCount;
    private int hourCount;

    int getCurrentSecond = 0;

    // Update is called once per frame
    void Update () {

        UpdateRealTime();

    }

    public void UpdateRealTime()
    {
        secondsCount += Time.deltaTime;

        if (secondsCount >= 60)
        {
            minuteCount++;
            secondsCount = 0;
        }
        else if (minuteCount >= 60)
        {
            hourCount++;
            minuteCount = 0;
        }

        BlinkStars();
    }

    private void BlinkStars()
    {
        getCurrentSecond = Mathf.RoundToInt(this.secondsCount);

        if (getCurrentSecond % 2 == 0)
        {
            starsImage.SetActive(true);

        }
        else if (getCurrentSecond % 2 == 1)
        {
            starsImage.SetActive(false);
        }
    }

}

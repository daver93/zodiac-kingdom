﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpLeftCorner : MonoBehaviour {

    public GameObject objectTarget;

    public Vector3 position2;

    public Camera camera;

    public PlayerController playerController;

    public GameObject aries;
    public GameObject aquarius;
    public GameObject libra;
    public GameObject sagittarius;
    public GameObject gemini;
    public GameObject leo;

    // Use this for initialization
    void Update()
    {
        objectTarget.transform.position = camera.ViewportToWorldPoint(position2);

        checkIfCollectedZodiacKey();

    }

    //this method checks if player collected a zodia key and if so, then activate it in the scene
    private void checkIfCollectedZodiacKey()
    {
        if (playerController.ariesKey == true)
        {
            aries.SetActive(true);
        }
        if (playerController.leoKey == true)
        {
            leo.SetActive(true);
        }
        if (playerController.aquariusKey == true)
        {
            aquarius.SetActive(true);
        }
        if (playerController.geminiKey == true)
        {
            gemini.SetActive(true);
        }
        if (playerController.libraKey == true)
        {
            libra.SetActive(true);
        }
        if (playerController.sagittariusKey == true)
        {
            sagittarius.SetActive(true);
        }
    }
}

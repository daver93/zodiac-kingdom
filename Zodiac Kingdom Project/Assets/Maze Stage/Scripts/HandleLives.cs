﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandleLives : MonoBehaviour {

    public int numberOfLives;

    public GameObject life01;
    public GameObject life02;
    public GameObject life03;

    public AudioSource playerAudioLostLife;

    // Use this for initialization
    void Start () {
        numberOfLives = 3;
	}

    public void LoseALife()
    {
        numberOfLives--;
        Debug.Log("A life lost: " + numberOfLives);
        playerAudioLostLife.Play();
        CheckLives();
    }

    public void GainALife()
    {
        numberOfLives++;
        Debug.Log("A life gained: " + numberOfLives);
        CheckLives();
    }

    private void CheckLives()
    {

        if (numberOfLives == 3)
        {
            life03.SetActive(true);

        }
        else if (numberOfLives == 2)
        {
            life02.SetActive(true);

            life03.SetActive(false);
        }
        else if (numberOfLives == 1)
        {
            life01.SetActive(true);

            life02.SetActive(false);
            life03.SetActive(false);
        }
        else if (numberOfLives == 0)
        {
            life01.SetActive(false);
            life02.SetActive(false);
            life03.SetActive(false);

            SceneManager.LoadScene("GameOver Scene");
        }
    }
}

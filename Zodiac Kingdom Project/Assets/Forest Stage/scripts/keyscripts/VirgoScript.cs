﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirgoScript : MonoBehaviour {
    public AudioSource MusicSource;

    public GameObject  virgo, virgo1;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D collision)
    {
        MusicSource.Play();

        virgo.gameObject.SetActive(true);

        virgo1.gameObject.SetActive(false);
        KeyScript.zwdia += 1;
    }
}

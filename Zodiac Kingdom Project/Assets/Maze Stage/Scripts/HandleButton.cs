﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleButton : MonoBehaviour {

    public bool isOpen;
    public Animator animator;

    private PlayButtonAudio playButtonAudio;

    // Use this for initialization
    void Start () {
        isOpen = true;

        playButtonAudio = GameObject.Find("ButtonAudioSource").GetComponent<PlayButtonAudio>();
    }

    //check for collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check if the collision is with the Player
        if (collision.tag == "Player")
        {
            ChangeButtonState();
        }
    }

    //change button to on and off vice versa
    private void ChangeButtonState()
    {
        playButtonAudio.PlaySound();

        this.isOpen = !this.isOpen;

        animator.SetBool("isOpen", this.isOpen);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour {
    
    public AudioSource MusicSource;
   
     void OnTriggerEnter2D(Collider2D collision)
    {
        MusicSource.Play();
        Player.health -=1;
    }
}

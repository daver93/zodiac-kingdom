﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpio1Script : MonoBehaviour {
    public AudioSource MusicSource;
    // Use this for initialization
    public GameObject scorpio, scorpio1;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D collision)
    {
         MusicSource.Play();
        scorpio.gameObject.SetActive(true);

        scorpio1.gameObject.SetActive(false);
        KeyScript.zwdia += 1;
    }
}
